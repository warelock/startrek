# startrek

# Overview

Console mode Star Trek game in AlphaBasic for VAM (Virtual Alpha Micro) PDP-11 clone emulator/AMOS operating system

![Screenshot](startrek.png)

The purpose of this "retrocomputing" project is to recreate one of my high school computing experiences.

## Manifest

| Filename | Description |
| :-: | :- |
| STAR2.BAS | The game itself, written in AlphaBasic |
| STRHLP.TXT | An in-game, instructional help file |
| tapeupdate | A Bash script to create a virtual tape of the game and help files |
| STAR.BAS | The original, broken, AlphaBasic game source code from my high school days (circa early 1980's) |
| STTR1.TXT | The original game source code by Mike Mayfield, of Centerline Engineering, which inspired so many game clones |

This game is designed to run on the VAM (Virtual Alpha Micro) emulator, by Mike Noel (http://www.kicksfortso.com/Am100/). The emulator simulates an Alpha Micro computer, which is a clone of a DEC PDP-11 computer. The Alpha Micro was popular in the 1970's and 1980's. My high school, Southwest High School in San Diego, had an Alpha Micro AM-100 computer system with 13 Soroc-brand dumb terminals.

---
## Manual installation instructions

* Windows: Install Cygwin with basic "gcc"-based development option and "ncurses" developer library

* Ubuntu Linux: Install the "build-essential" software package and "ncurses" developer library

* Clone the "startrek" github repo, downloading a copy to your local PC

* Download the [VAM emulator software](http://www.kicksfortso.com/Am100/) and install it into the local "startrek" repo directory

* Download a copy of the AMOS operating system main DSK0: hard drive virtual disk pack file and rename it to "dsk0-container"

* Unpack a copy of the "empty.tgz" compressed archive and rename it to "dsk1-container"

* Modify the "am100.ini" file's "am600" section, adding "file='tape.aws'" to the end of that line

* Use the "tapeupdate" bash script to package the game files into a virtual tape file named "tape.aws"

* Start the VAM emulator

```bash
./am100
```

* When you see the main "SYSTAT" display on the main "JOB1" virtual terminal, break out of it with a "Control-C" key sequence

* Use the "LOG 1,2" command to switch to the administrator account of the VAM

```
LOG 1,2
```

* Create a "100,1" account on the secondary "DSK1:" hard drive

```
SYSACT DSK1:
```

* Use the "ALT-2" key sequence to switch to the "JOB2" virtual terminal, where you will see a "." AMOS command prompt

* Log into the newly created "100,1" user account:

```
LOG 100,1
```

* Get a listing of the virtual data storage tape (entering nothing at the prompt, to take the default tape drive)

```
TAPDIR
```

* Load the game from the virtual data storage tape into the "100,1" account (entering nothing at the prompt, to take the default tape drive)

```
TAPFIL =
```

* Get a listing of the newly loaded files

```
DIR
```

* Compile and run the game

```
COMPIL STAR2
RUN STAR2
```

* When you're done using the emulator, use the "ALT-F" and "<Enter>" key sequence to tell VAM to stop running (Use a "<Tab>" key sequence to switch between VAM prompt options)
